export const useSave = async (
  key,
  form,
  entities,
  dispatch,
  updateEntityThunk,
  setEditingKey,
) => {
  try {
    const row = await form.validateFields();
    const newData = [...entities];
    const index = newData.findIndex((item) => key === item._id);

    if (index > -1) {
      const item = newData[index];
      newData.splice(index, 1, { ...item, ...row });
      row.coordinate = row.coordinate
        .split(",")
        .map((item) => parseFloat(item.trim()));
      row.label = row.label.split(",").map((item) => item.trim());
      dispatch(updateEntityThunk(newData[index]));
      setEditingKey("");
    } else {
      newData.push(row);
      setEditingKey("");
    }
  } catch (errInfo) {
    console.log("Validate Failed:", errInfo);
  }
};
