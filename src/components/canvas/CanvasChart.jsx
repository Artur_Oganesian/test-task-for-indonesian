import React, { useRef, useEffect } from "react";

const CanvasChart = ({ entities }) => {
  const canvasRef = useRef(null);

  useEffect(() => {
    const canvas = canvasRef.current;
    const context = canvas.getContext("2d");

    canvas.width = 800;
    canvas.height = 600;

    context.clearRect(0, 0, canvas.width, canvas.height);

    context.fillStyle = "white";
    context.fillRect(0, 0, canvas.width, canvas.height);

    const numLines = canvas.height / 25;
    const lineSpacing = canvas.height / numLines;
    context.strokeStyle = "lightgreen";
    context.fillStyle = "black";
    context.font = "12px Arial";

    for (let i = 1; i < numLines; i++) {
      const y = i * lineSpacing;

      // Draw grid line
      context.beginPath();
      context.moveTo(0, y);
      context.lineTo(canvas.width, y);
      context.stroke();

      const label = (numLines - i) * 5;
      context.fillText(label.toString(), 5, y - 5);
    }

    context.fillStyle = "blue";
    context.strokeStyle = "black";
    entities.forEach((entity) => {
      const [x, y] = entity.coordinate.map(Number);

      if (!isNaN(x) && !isNaN(y)) {
        context.beginPath();
        context.arc(x, y, 5, 0, 2 * Math.PI);
        context.fill();
        context.stroke();

        entity.label.forEach((label, index) => {
          context.fillText(label, x + 10, y + 20 * index);
        });
      } else {
        console.warn(`Invalid coordinates for entity: ${entity.name}`);
      }
    });
  }, [entities]);

  return (
    <canvas
      ref={canvasRef}
      style={{ border: "1px solid black", position: "relative" }}
      className="w-full h-full"
    />
  );
};

export default CanvasChart;
