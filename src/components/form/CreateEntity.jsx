import React from "react";
import { Form, Input, Spin } from "antd";
import { useSelector } from "react-redux";
import { Button, Modal } from "antd";

const CreateEntity = ({ onClick, onCancel, visible, onOk, form }) => {
  const createLoading = useSelector(
    (state) => state.entitiesStored.entityEvents.create.isLoading,
  );

  return (
    <Spin tip="Loading..." spinning={createLoading}>
      <div className="flex justify-end my-5">
        <Button loading={createLoading} type="primary" onClick={onClick}>
          Create Entity
        </Button>

        <Modal
          title="Create Entity"
          open={visible}
          onOk={onOk}
          onCancel={onCancel}
          confirmLoading={createLoading}
        >
          <Form form={form} layout="vertical" name="form_in_modal">
            <Form.Item
              name="name"
              label="Name"
              rules={[
                {
                  required: true,
                  message: "Please input the name of the entity!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="coordinate"
              label="Coordinate"
              rules={[
                {
                  required: true,
                  message:
                    "Please input the coordinate as a comma-separated list of numbers!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="label"
              label="Label"
              rules={[
                {
                  required: true,
                  message: "Please input the label!",
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Form>
        </Modal>
      </div>
    </Spin>
  );
};

export default CreateEntity;
