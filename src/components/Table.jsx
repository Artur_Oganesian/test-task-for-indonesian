


import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getEntityThunk,
  deleteEntityThunk,
  updateEntityThunk,
  createEntityThunk,
} from "../globalRedux/slices/entities";
import {
  Form,
  Input,
  InputNumber,
  Popconfirm,
  Table as AntTable,
  Typography,
} from "antd";
import CreateEntity from "./form/CreateEntity";
import CanvasChart from "./canvas/CanvasChart"; 

const EditableCell = ({
  editing,
  dataIndex,
  title,
  inputType,
  record,
  index,
  children,
  ...restProps
}) => {
  const inputNode = inputType === "number" ? <InputNumber /> : <Input />;
  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          style={{ margin: 0 }}
          rules={[{ required: true, message: `Please Input ${title}!` }]}
        >
          {inputNode}
        </Form.Item>
      ) : (
        children
      )}
    </td>
  );
};

const Table = () => {
  const [form] = Form.useForm();
  const [editingKey, setEditingKey] = useState("");
  const [isModalVisible, setIsModalVisible] = useState(false);
  const dispatch = useDispatch();

  const entities = useSelector((state) => state.entitiesStored.entitiesData);
  const updateLoading = useSelector((state) => state.entitiesStored.entityEvents.update.isLoading);
  const createLoading = useSelector((state) => state.entitiesStored.entityEvents.create.isLoading);
  const deleteLoading = useSelector((state) => state.entitiesStored.entityEvents.delete.isLoading);

  useEffect(() => {
    dispatch(getEntityThunk());
  }, [dispatch, updateLoading, createLoading, deleteLoading]);

  const isEditing = (record) => record.id === editingKey;

  const edit = (record) => {
    form.setFieldsValue({
      name: record.name,
      coordinate: record.coordinate.join(", "),
      label: record.label.join(", "),
    });
    setEditingKey(record.id);
  };

  const cancel = () => {
    setEditingKey("");
  };

  const save = async (id) => {
    try {
      const row = await form.validateFields();
      const updatedEntity = {
        ...row,
        coordinate: row.coordinate.split(",").map((item) => parseFloat(item.trim())),
        label: row.label.split(",").map((item) => item.trim())
      };
      dispatch(updateEntityThunk({ ...updatedEntity, id: id }));
      setEditingKey("");
    } catch (errInfo) {
      console.log('Validate Failed:', errInfo);
    }
  };

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      width: "25%",
      editable: true,
    },
    {
      title: "Coordinate",
      dataIndex: "coordinate",
      width: "15%",
      editable: true,
      render: (text) => (Array.isArray(text) ? text.join(", ") : text),
    },
    {
      title: "Label",
      dataIndex: "label",
      width: "40%",
      editable: true,
      render: (text) => (Array.isArray(text) ? text.join(", ") : text),
    },
    {
      title: "Operation",
      dataIndex: "operation",
      render: (_, record) => {
        const editable = isEditing(record);
        return editable ? (
          <span>
            <Typography.Link
              onClick={() => save(record.id)}
              style={{ marginRight: 8 }}
            >
              Save
            </Typography.Link>
            <Popconfirm title="Sure to cancel?" onConfirm={cancel}>
              <a>Cancel</a>
            </Popconfirm>
          </span>
        ) : (
          <div className="flex gap-4">
            <Typography.Link
              disabled={editingKey !== ""}
              onClick={() => edit(record)}
            >
              Edit
            </Typography.Link>
            <Popconfirm
              title="Are you sure to delete this entity?"
              onConfirm={() => dispatch(deleteEntityThunk(record.id))}
            >
              <Typography.Link className="!text-[red]">Delete</Typography.Link>
            </Popconfirm>
          </div>
        );
      },
    },
  ];

  const mergedColumns = columns.map((col) => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record) => ({
        record,
        inputType: col.dataIndex === "coordinate" ? "text" : "text",
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    form
      .validateFields()
      .then((values) => {
        values.coordinate = values.coordinate.split(",").map((item) => parseFloat(item.trim()));
        values.label = values.label.split(",").map((item) => item.trim());
        dispatch(createEntityThunk(values));
        setIsModalVisible(false);
        form.resetFields();
      })
      .catch((info) => {
        console.log("Validate Failed:", info);
      });
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const updatedEntities = entities.map(({ name, coordinate, label, id }) => {
    const updatedCoord = coordinate.join(",").split(",").map((item) => parseFloat(item.trim()));
    const updatedLabel = label.join(",").split(",").map((item) => item.trim());
    return {
      id,
      name,
      coordinate: updatedCoord,
      label: updatedLabel,
    };
  });

  return (
    <>
      <Form form={form} component={false}>
        <AntTable
          components={{
            body: {
              cell: EditableCell,
            },
          }}
          bordered
          dataSource={entities}
          columns={mergedColumns}
          rowClassName="editable-row"
          pagination={false}
          rowKey="id"
        />
      </Form>
      <CreateEntity
        onClick={showModal}
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        form={form}
      />

      <CanvasChart entities={updatedEntities} />
    </>
  );
};

export default Table;
