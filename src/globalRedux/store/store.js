import { configureStore } from "@reduxjs/toolkit";
import entitiesRedcuer from "../slices/entities";

export const store = configureStore({
  reducer: {
    entitiesStored: entitiesRedcuer,
  },
});
