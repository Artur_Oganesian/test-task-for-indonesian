


import axios from "axios";

const baseURL = "https://6656d8589f970b3b36c6c63e.mockapi.io/entities";

export const EntityService = {
  getEntities: async () => {
    try {
      const { data } = await axios.get(baseURL);
      return data;
    } catch (err) {
      console.error(err.message);
    }
  },

  deleteEntity: async (id) => {
    try {
      await axios.delete(`${baseURL}/${id}`);
    } catch (err) {
      console.error(err.message);
    }
  },
  
  updateEntity: async (id, entity) => {
    try {
      const response = await axios.put(`${baseURL}/${id}`, entity);
      return response.data;
    } catch (err) {
      console.error(err.message);
    }
  },
  
  createEntity: async (entity) => {
    try {
      const response = await axios.post(baseURL, entity);
      return response.data;
    } catch (err) {
      console.error(err.message);
    }
  },
};
