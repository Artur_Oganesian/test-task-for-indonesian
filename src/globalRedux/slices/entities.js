


import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { EntityService } from "../services/entities";

export const getEntityThunk = createAsyncThunk("entities/getEntities", async () => {
  const entitiesData = await EntityService.getEntities();
  return entitiesData;
});

export const deleteEntityThunk = createAsyncThunk("entities/deleteEntity", async (id) => {
  await EntityService.deleteEntity(id);
  return id;
});

export const updateEntityThunk = createAsyncThunk("entities/updateEntity", async (entity, thunkAPI) => {
  try {
    const response = await EntityService.updateEntity(entity.id, entity);
    return response;
  } catch (error) {
    return thunkAPI.rejectWithValue(error.response.data);
  }
});

export const createEntityThunk = createAsyncThunk("entities/createEntity", async (entity, thunkAPI) => {
  try {
    const response = await EntityService.createEntity(entity);
    return response;
  } catch (error) {
    return thunkAPI.rejectWithValue(error.response.data);
  }
});

const initialState = {
  entitiesData: [],
  isLoading: false,
  entityEvents: {
    delete: {
      isLoading: false,
    },
    update: {
      isLoading: false,
    },
    create: {
      isLoading: false,
    },
  },
};

const entitySlice = createSlice({
  name: "entities",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getEntityThunk.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getEntityThunk.fulfilled, (state, action) => {
        state.isLoading = false;
        state.entitiesData = action.payload;
      })
      .addCase(getEntityThunk.rejected, (state) => {
        state.isLoading = false;
      })
      .addCase(deleteEntityThunk.pending, (state) => {
        state.entityEvents.delete.isLoading = true;
      })
      .addCase(deleteEntityThunk.fulfilled, (state, action) => {
        state.entityEvents.delete.isLoading = false;
        state.entitiesData = state.entitiesData.filter(entity => entity.id !== action.payload);
      })
      .addCase(deleteEntityThunk.rejected, (state) => {
        state.entityEvents.delete.isLoading = false;
      })
      .addCase(updateEntityThunk.pending, (state) => {
        state.entityEvents.update.isLoading = true;
      })
      .addCase(updateEntityThunk.fulfilled, (state, action) => {
        state.entityEvents.update.isLoading = false;
        state.entitiesData = state.entitiesData.map(entity => entity.id === action.payload.id ? action.payload : entity);
      })
      .addCase(updateEntityThunk.rejected, (state) => {
        state.entityEvents.update.isLoading = false;
      })
      .addCase(createEntityThunk.pending, (state) => {
        state.entityEvents.create.isLoading = true;
      })
      .addCase(createEntityThunk.fulfilled, (state, action) => {
        state.entityEvents.create.isLoading = false;
        state.entitiesData.push(action.payload);
      })
      .addCase(createEntityThunk.rejected, (state) => {
        state.entityEvents.create.isLoading = false;
      });
  },
});

export default entitySlice.reducer;
