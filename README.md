# React + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh


Entity Management System
Overview
This project is an Entity Management System built with React and Redux, utilizing Ant Design for the UI components. It allows users to create, read, update, and delete (CRUD) entities, with each entity having a name, coordinates, and labels. The entities are displayed in an editable table, and their positions are visualized on a canvas chart.

Project Structure
bash
Copy code
/src
  /components
    CanvasChart.js
    CreateEntity.js
    Table.js
  /customHooks
    useSave.js
  /globalRedux
    /slices
      entities.js
    store.js
  /services
    entities.js
  App.js
  index.js
Components
CanvasChart.js

A React component that draws entities on a canvas.
Displays a grid and plots each entity based on its coordinates.
CreateEntity.js

A form modal component for creating new entities.
Utilizes Ant Design's form components for input validation and submission.
Table.js

Main component that displays entities in an editable table.
Supports editing, deleting, and creating entities.
Integrates with Redux for state management and dispatching actions.
Custom Hooks
useSave.js
A custom hook to handle the save operation for entity updates.
Dispatches the updateEntityThunk action to update the entity in the Redux store and backend.
Global Redux
slices/entities.js

Defines the Redux slice for managing entities.
Contains asynchronous thunks for fetching, creating, updating, and deleting entities.
Manages the loading state and entity data in the Redux store.
store.js

Configures the Redux store and combines all slices.
Services
entities.js
Service module that defines API calls for fetching, creating, updating, and deleting entities using Axios.
Entry Points
App.js

Main application component that includes the table and canvas chart.
Provides a cohesive user interface for managing and visualizing entities.
index.js

Entry point of the React application.
Sets up the Redux provider and renders the App component.
Usage
Running the Project
Install Dependencies:

Copy code
npm install
Start the Development Server:

sql
Copy code
npm start
Build for Production:

arduino
Copy code
npm run build
Interacting with the Application
Viewing Entities:

The entities are fetched from the mock API and displayed in a table.
Creating an Entity:

Click the "Add Entity" button to open the modal form.
Fill in the name, coordinates, and labels.
Click "Save" to create the entity.
Editing an Entity:

Click the "Edit" link in the table row of the entity you want to edit.
Modify the fields as needed and click "Save".
Click "Cancel" to discard changes.
Deleting an Entity:

Click the "Delete" link in the table row of the entity you want to remove.
Confirm the deletion in the pop-up dialog.
Visualizing Entities:

The entities are plotted on a canvas based on their coordinates.
The canvas updates automatically when entities are created, edited, or deleted.
Technical Details
Editable Table
The table uses Ant Design's Table component with custom editable cells.
The EditableCell component handles rendering of form fields within table cells.
The isEditing function checks if a row is currently being edited.
The save function validates the form and dispatches the updateEntityThunk action.
Canvas Chart
The CanvasChart component uses the HTML5 Canvas API to draw the grid and plot entities.
Entities are represented by blue dots, and their labels are displayed next to them.
The canvas updates whenever the entities prop changes.
Redux Thunks
getEntityThunk: Fetches entities from the mock API.
createEntityThunk: Creates a new entity via the mock API.
updateEntityThunk: Updates an existing entity.
deleteEntityThunk: Deletes an entity by ID.
API Service
The EntityService module defines API calls using Axios.
Base URL for the mock API is set up, and each function handles specific CRUD operations.
Mock API
Configuration
The project uses a mock API platform to simulate backend operations.
Ensure the mock API endpoints are correctly set in the entities.js service module.
Example Endpoints
Fetch Entities: https://6656d8589f970b3b36c6c63e.mockapi.io/entities
Create Entity: https://6656d8589f970b3b36c6c63e.mockapi.io/entities
Update Entity: https://6656d8589f970b3b36c6c63e.mockapi.io/entities/:id
Delete Entity: https://6656d8589f970b3b36c6c63e.mockapi.io/entities/:id
Conclusion
This project provides a comprehensive system for managing and visualizing entities, utilizing React, Redux, and Ant Design. The integration with a mock API allows for testing and development without needing a live backend. Future improvements could include adding more sophisticated validation, enhancing the canvas chart with interactivity, and optimizing performance for large datasets.